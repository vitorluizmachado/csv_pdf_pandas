#!/usr/bin/python
# -*- coding: UTF-8 -*-

import pandas as pd
from os import path, makedirs
import glob
from fpdf import FPDF
from pathlib import Path

fontfolder = Path('fonts/Roboto/')



intervalo=5000
start=1
tamanho=5_000
#db = pd.read_csv('Sisap_Completo2.csv', delimiter=',',encoding='ISO-8859-1',header=0, dtype='str')
db = pd.read_csv('Sisap_Completo2.csv', delimiter=',', header=0, dtype='str')

final=len(db)
dif_calc = [0]
last_row = db.iloc[0, :]

for i in range(start,tamanho):
    pdf = FPDF('P','mm','A4')
    pdf.add_font('Roboto-Regular', '', fontfolder/'Roboto-Regular.ttf')
    
    count=start
    for count, row in db.iloc[count-1:, :].iterrows():

        pdf.add_page()
        pdf.set_font("Roboto-Regular", '', 8)
        pdf.image('fverso.jpg', x=0, y=0, w=round(pdf.w), h=round(pdf.h))
        pdf.text(x=60,y=250, txt=f"{row[0]} {row[1]} ")
        pdf.text(x=60,y=254, txt=f"{row[2]},{row[3]} - {row[4]}. CEP: {row[5]}")
        if count == tamanho-1:
            break
        count+=1
    pdf.output(f"prontos/Mala-{str(start).zfill(6)}-{str(count+1).zfill(6)}.pdf")
    if count == final:
        break
    start+=intervalo
    tamanho+=intervalo
    